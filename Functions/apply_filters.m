% Apply filters on Bacmman measurement tables
%
% Example of filter set-up in main input (for SOS signal > 1000 and cell length > 4 um):
% filters.var_to_filter = {'MeanSOS', 'SpineLength'};
% filters.source_table = [0, 0];
% filters.comparison_method = {'>', '>'}; % '>', '<', '=' (e.g. '>' to keep var > threshold)
% filters.filter_threshold = [1000, 4];
%

function varargout = apply_filters(filters, varargin)

if (length(filters.var_to_filter) + length(filters.source_table) + length(filters.filter_threshold) + length(filters.comparison_method))/4 ~= length(filters.var_to_filter)
    error('Number of variables set for filtering is inconsistent')
end

if nargout ~= length(varargin)
    error('apply_filters: number of output tables must be equal to the number of input tables')
end

nTables = 1:length(varargin);

for i = 1:length(filters.var_to_filter)
    disp(['Filtering data for ' filters.var_to_filter{i} ' ' filters.comparison_method{i} ' ' num2str(filters.filter_threshold(i))])
    
    % Define source and downstream tables
    table_level = zeros(length(varargin),1);
    for j = 1:length(varargin)
        table_level(j) = sum(cell2mat(strfind(varargin{j}.Properties.VariableNames, 'Indices'))) - 2; % Parents are level 0, children will be level 1, 2,...
    end
    source = varargin{nTables == filters.source_table(i)+1};
    downstream_idx = nTables(table_level >= table_level(filters.source_table(i)+1)); % Only apply filter to children tables
    for j = 1:length(downstream_idx)
        downstream{j} = varargin{downstream_idx(j)};
    end
    
    % Filter source table
    switch filters.comparison_method{i}
        case '>'
            source = source(table2array(source(:,filters.var_to_filter(i))) > filters.filter_threshold(i), :);
        case '<'
            source = source(table2array(source(:,filters.var_to_filter(i))) < filters.filter_threshold(i), :);
        case '='
            source = source(table2array(source(:,filters.var_to_filter(i))) == filters.filter_threshold(i), :);
    end
    
    % Filter downstream tables (if any) on the basis of FOV number + cell number
    for j = 1:length(downstream_idx)
        % Indices_2 is cell number
        downstream{j} = downstream{j}(ismember([downstream{j}.PositionIdx downstream{j}.Indices_2], [source.PositionIdx source.Indices_2], 'rows'),:);
    end
    
    % Store filtered tables
    varargin{nTables == filters.source_table(i)+1} = source;
    for j = 1:length(downstream_idx)
        varargin{downstream_idx(j)} = downstream{j};
    end
    
end

varargout = varargin;

end


