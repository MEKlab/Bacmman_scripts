%%% Import data from Bacmman measurements tables
%%%
%%% Requirements:
%%% Bacteria measurements table (from Bacmman)
%%% Indices of tables to import in array format (e.g. [0,1])
%%%

function varargout = import_measurements(folder, indices)

if nargout ~= length(indices)
    error('Number of output variables for import_measurements must be equal to the number of object indices given')
end

folder = fullfile(folder);
if ~strcmp(folder(end), filesep)
    folder = [folder filesep];
end

dsIdx = strfind(folder, filesep);
dsName = folder(dsIdx(end-1)+1:dsIdx(end)-1);

measurements = dir([folder dsName '*.csv']);

if length(measurements) < length(indices)
    error(['Measurement tables are missing in ' folder])
end

for i = 1:length(indices)
    varargout{i} = readtable([folder measurements(indices(i)+1).name], 'TreatAsEmpty', 'NA');
    if ismember('Indices', varargout{i}.Properties.VariableNames)
        if ~isempty(varargout{i})
            varargout{i}.IndicesStr = varargout{i}.Indices;
            [varargout{i}.Indices] = str2double(split(varargout{i}.Indices, '-'));
            varargout{i} = splitvars(varargout{i});
        else
            warning(['Table ' num2str(indices(i)) ' was empty'])
        end
    end
end

end