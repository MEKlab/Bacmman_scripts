% Add info from metadata files to imported measurement tables
% Folder: Bacmman folder for the dataset
% channels: the channel number for the specified metadata (format: array of integers)
% metadata: metadata parameter keys to be added to the measurements (format: cell array of strings)
% varargin: Measurement tables to add the metadata info to

function varargout = add_metadata_info(folder, varToAdd, channels, varargin)

warning('off') % Disable warnings for the length of the function to avoid annoying table warnings

if length(channels) ~= length(varToAdd)
    error('add_metadata_info: each metadata field must have a matching channel index')
end

if ~strcmp(folder(end),'/') && ~strcmp(folder(end),'\')
    folder = [folder '/'];
end

uChannels = unique(channels);
uPositions = unique(varargin{1}.Position);

for i = 1:length(uPositions) % For each FOV
    
    for j = 1:length(uChannels) % For each channel
        
        % Variables associated with this channel
        currentVar = varToAdd(channels == uChannels(j));
        
        % Import metadata file
        fileID = fopen([folder 'SourceImageMetadata/' char(uPositions(i)) '_c' num2str(channels(j)) '.txt']);
        metadata = textscan(fileID, '%s%s', 'Delimiter', '='); % Use textscan for this because it's faster than readtable
        
        for k = 1:length(currentVar) % For each metadata variable to import
            
            % Check that variable exists
            if ~sum(strcmp(metadata{1,1}, currentVar(k)))
                error(['Variable "' cell2mat(currentVar(k)) '" not found. Check that it is spelled correctly? Names are case-sensitive.'])
            end
            
            for m = 1:length(varargin) % For each table that needs updating
                
                lines = strcmp(varargin{m}.Position, uPositions(i)); % Lines corresponding to the current image
                newVarName = matlab.lang.makeValidName(strjoin([currentVar(k), '_c' num2str(uChannels(j))], '')); % Variable to be added
                if sum(lines)
                    varargin{m}(lines, newVarName) = metadata{1,2}(strcmp(metadata{1,1}, currentVar(k)));
                end
                
            end
            
        end
        
        fclose(fileID);
        
    end
    
end

varargout = varargin;

warning('on')

end