% Create the requested number of figures and return figure handles
% Adjusts position so that the figures don't overlap
%
% Maximum number of figures: 6


function varargout = Init_figures

n_fig = nargout;
col = [0 560 1120];
lin = [558 54];
dim = [560 420];

% An alternative to this is "pos = combvec(col, lin)" but it requires the deep-learning toolbox
[a, b] = ndgrid(col, lin);
pos = [a(:), b(:)];

for i = 1:n_fig
    varargout{i} = figure('Color', 'white', 'Position', [pos(i,:) dim]);
    hold on
end

end