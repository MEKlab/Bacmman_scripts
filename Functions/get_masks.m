function varargout = get_masks(folder, filename, varargin)

if length(varargin) < 1
    error('get_masks requires at least a path, filename and 1 feature name to be extracted')
end

if nargout ~= length(varargin)
    error('Number of output variables for get_masks must be equal to the number of extracted features')
end

if ~strcmp(folder(end),'/') && ~strcmp(folder(end),'\')
    folder = [folder '/'];
end
info = h5info([folder filename]);
nImages = length(info.Groups(1).Groups.Groups);

for i = 1:nImages
    
    dataset = info.Groups(1).Groups.Groups(i).Name;
    
    for j = 1:length(varargin)
        
        feature = varargin{j};
        
        try
            data = h5read([folder filename],...
                [dataset '/' feature]);
        catch
            error(['Could not find feature "' feature '"'])
        end
        
        varargout{j}(:,:,i) = data(:,:,1);
        
    end
    
end


end