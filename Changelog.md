Changelog - Bacmman_scripts

# v0.1.1 - 21/03/2022

**apply_filters**: changed behaviour.
If filtering a child object, parents will be unaffected (e.g. if filtering "spots" that are contained in "bacteria", no bacteria will be deleted).
The opposite is still true (removing a parent object will remove all its children objects).

**New script: Create_selection**
Creates a selection of objects that can be visualised in Bacmman. The selection can be made from filters (e.g. only small cells, high SOS intensity,...).

# v0.1 - 17/03/2022

**get_masks**
Now imports features for all images in the dataset at once (instead of one image at a time).
Output is now a 3D matrix. If images are of size n\*m and there are k images, the matrix will be of size n\*m\*k (for each feature).
Modified calling sequence accordingly: `[obj1, obj2,...] = get_masks(folder, filename, 'FeatureName1', 'FeatureName2',...)`
`folder` no longer needs to end with a slash.

**New script: import_measurements**
Imports Bacmman .csv measurement tables.
Calling sequence: `[obj1, obj2,...] = import_measurements(folder, [idx1, idx2,...])` where `idx` is the index of the measurement table to be imported.
For example, use [0,1] to import tables 0 and 1.

**New script: add_metadata_info**
Reads image metadata saved by Bacmman (can be found in the Bacmman folder, under 'SourceImageMetadata') and adds the specified fields to the measurements tables. Add to preamble:
```
varToAdd = {}; % Metadata variables to add (e.g. 'DateTime')
channels = []; % Index of table containing varToAdd
```
Calling sequence: `[table1, table2,...] = add_metadata_info(folder, varToAdd, channels, table1, table2,...)`
With:
- folder: Bacmman folder
- varToAdd: key values of metadata fields to be added to the tables, in a cell array. Example: {'Exposure', 'DateTime'}
- channels: the channel index where the metadata should be read, as an array. Example: [0, 0] or [0,1].
Note that a given metadata key can have different values in different channels (e.g. 'Exposure' would be different between brightfield and fluorescence channels)
- table1, table2,... are the measurement tables (import with `import_measurements`) to which the metadata should be added.

**New script: apply_filters**
Applies user-defined filters on measurement tables. To use this function, define the filters like this in your preamble:
```
filters.var_to_filter = {'MeanSOS', 'SpineLength'};
filters.source_table = [0, 0];
filters.comparison_method = {'>', '>'}; % '>', '<', '=' (e.g. '>' to keep var > threshold)
filters.filter_threshold = [1000, 4];
```
The number of elements must be the same in every array.
To use no filtering, leave all arrays empty.
To call the function, use: `[table1, table2,...] = apply_filters(filters, table1, table2,...)`

**New scripts: Plot_SOS_histograms and Plot_cell_length**
Scripts to plot SOS signal or cell length histograms, and their evolution in time during data acquisition.
In the input, give the paths to Bacmman folders to be processed as a cell array in `datasets`. Several folders can be given to overlay the curves.
