% Make histograms of SOS signal and SOS signal vs. time
%
% Required measurements:
% *ObjectFeatures* -> Object class: Bacteria -> Feature: mean -> Intensity: SOS -> Name: MeanSOS
%

cd(fileparts(matlab.desktop.editor.getActiveFilename)) % cd to script's current folder
addpath('../Functions/')

close all
clc

%% Input

% Paths to Bacmman folders
datasets = {'D:/Daniel Th�di�/BACMMAN\Timelapse\220202_AL140_nocipro',...
            'D:/Daniel Th�di�/BACMMAN\Timelapse\220302_cipro30ngmL'};

%% Options

% Metadata (will be added to measurements table and can be used for filtering)
varToAdd = {'DateTime'}; % Metadata variables to add (e.g. 'DateTime')
channels = [0]; % Index of table containing var_to_add

% Filters (look only at a subset of the data)
% Multiple filters can be defined using arrays; leave arrays empty to apply no filter
filters.var_to_filter = {}; % Measurement names (e.g. 'SpineLength')
filters.source_table = []; % Index of the table that contains "var_to_filter"
filters.comparison_method = {}; % '>', '<', '=' (e.g. '>' to keep var > threshold)
filters.filter_threshold = []; % Threshold value for the filters

% Bins for SOS histogram (start:step:end)
binRange = 0:10:3000;

% Define a colormap for the figures (must have at least as many colors as datasets)
cmap = [[255 193 7];...
        [30 136 229];...
        [0 77 64];...
        [216 27 96]]./255;


%% Processing

% Initialise figures
[f1, f2] = Init_figures;
p1 = [];
p2 = [];

for a = 1:length(datasets)

    b = strfind(datasets{a}, filesep);
    dataset_name{a} = datasets{a}(b(end)+1:end);
    disp(['Loading dataset ' dataset_name{a} '...'])


    %% Import and filter data
    bact = import_measurements(datasets{a}, 0);

    % Add metadata
    bact = add_metadata_info(datasets{a}, varToAdd, channels, bact);

    % Apply user-defined filters
    bact = apply_filters(filters, bact);

    %% Data processing

    % Convert time values
    bact.DateTime_c0 = datetime(bact.DateTime_c0, 'InputFormat', 'yyyyMMdd HH:mm:ss.SSS');
    timepoints = unique(bact.DateTime_c0 - bact.DateTime_c0(1));

    % Compute average SOS signal per image
    sos = grpstats(bact.MeanSOS, bact.PositionIdx, 'mean');
    sos_movmean = movmean(sos, 50);

    %% General statistics
    disp(['Average SOS signal of dataset: ' num2str(mean(bact.MeanSOS),'%.0f')])

    %% Make figures

    % Histogram of SOS signal
    p1(a,:) = histcounts(bact.MeanSOS,[binRange Inf], 'Normalization', 'probability');

    % SOS signal vs time
    figure(f2)
    scatter(minutes(timepoints), sos, 15, 'filled', 'MarkerFaceAlpha', 0.8, 'MarkerFaceColor', cmap(a,:));
    p2(a) = plot(minutes(timepoints), sos_movmean, 'LineWidth', 1.5, 'Color', cmap(a,:));
    box on
    xlabel('Time (min)')
    ylabel('Average SOS signal')
    drawnow

    fprintf('\n')

end

% Update figures
figure(f1)
b = plot(binRange(1:end-1), p1(:,1:end-1), 'LineWidth', 1);
for i = 1:length(dataset_name)
    set(b(i),'Color', cmap(i,:));
end
xlabel('Mean SOS signal per cell')
ylabel('PDF')
hold off
legend(dataset_name, 'Interpreter', 'none')
legend('boxoff')


figure(f2)
hold off
legend(p2, dataset_name, 'Interpreter', 'none')
legend('boxoff')
