# Bacmman_scripts

This is a collection of Matlab scripts that can be used to read data exported from Bacmman/Talissman.
For details on how to export data from Bacmman and how to use the scripts, please refer to the wiki: https://www.wiki.ed.ac.uk/display/ElKarouiLab/Talissman+information

To use the scripts, download the repository and add it to your script using the command:
```
addpath(genpath('path/to/Bacmman_scripts'))
```

## Minimal script example: data import + filtering

The script below performs the following:
- Import measurements of cells and spots (exported as .csv by Bacmman, assuming here that they correspond to tables 0 and 1, respectively)
- Add the "DateTime" column to both tables, containing the exact time at which each image was acquired
  - Converts the "DateTime" column to date format
- Applies a filter to only keep cells longer than 1 µm and shorter than 4 µm

```
addpath(genpath('path/to/Bacmman_scripts'))

%% Input

% Path to Bacmman folder (contains the measurements)
dataset = {'path/to/Bacmman_folder'};

%% Options

% Metadata (will be added to measurements table and can be used for filtering)
varToAdd = {'DateTime'}; % Metadata variables to add (e.g. 'DateTime')
channels = [0]; % Index of channel containing varToAdd

% Filters
% Multiple filters can be defined using arrays; leave arrays empty to apply no filter
filters.var_to_filter = {'SpineLength', 'SpineLength'}; % Measurement names (e.g. 'SpineLength')
filters.source_table = [0, 0]; % Index of the table that contains "var_to_filter"
filters.comparison_method = {'>', '<'}; % '>', '<', '=' (e.g. '>' to keep var > threshold)
filters.filter_threshold = [1, 4]; % Threshold value for the filters

%% Import and filter data
[bact, spots] = import_measurements(dataset, [0, 1]);

% Add metadata
[bact, spots] = add_metadata_info(dataset, varToAdd, channels, bact, spots);
bact.DateTime_c0 = datetime(bact.DateTime_c0, 'InputFormat', 'yyyyMMdd HH:mm:ss.SSS'); % Convert to date format
spots.DateTime_c0 = datetime(spots.DateTime_c0, 'InputFormat', 'yyyyMMdd HH:mm:ss.SSS');

% Apply user-defined filters
[bact, spots] = apply_filters(filters, bact, spots);
```
