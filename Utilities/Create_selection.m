% Create and save a selection of the data that can be visualised in Bacmman

cd(fileparts(matlab.desktop.editor.getActiveFilename)) % cd to script's current folder
addpath('../Functions/')

close all
clc

%% Input

folder = 'D:\Daniel Th�di�\BACMMAN\Timelapse/220310_cipro0/';

%% Options

% Metadata (will be added to measurements table and can be used for filtering)
varToAdd = {}; % Metadata variables to add (e.g. 'DateTime')
channels = []; % Index of table containing var_to_add

% Filters
% Multiple filters can be defined using arrays; leave arrays empty to apply no filter
filters.var_to_filter = {'NucleoidCount'}; % Measurement names (e.g. 'SpineLength')
filters.comparison_method = {'='}; % '>', '<', '=' (e.g. '>' to keep var > threshold)
filters.filter_threshold = [0]; % Threshold value for the filters

% Selection options
object_index = 0; % Index of the Bacmman object to make the selection on
selection_name = 'One_nucleoid';
display_color = 'Cyan'; % Cyan, Blue, Yellow, Magenta, Orange, Green, Gray


%% Import and filter data

folder = fullfile(folder); % Normalise fileseps

% Import tables
data = import_measurements(folder, object_index);

% Add metadata
data = add_metadata_info(folder, varToAdd, channels, data);

% Apply user-defined filters
switch length(filters.var_to_filter)
    case 0
        filters.source_table = [];
    otherwise
        filters.source_table = zeros(1, length(filters.var_to_filter));
end
data = apply_filters(filters, data);

if isempty(data)
    disp('Selection is empty so it was not exported') % Empty selections are pointless and make Bacmman crash
else
    %% Create selection
    fields = unique(data.Position);
    
    output = struct;
    output.name = selection_name;
    output.structureIdx = object_index;
    output.color = display_color;
    for i = 1:length(unique(data.Position))
        output.objects.(fields{i}) = data.IndicesStr(strcmp(data.Position, fields{i}));
    end
    
    if strcmp(folder(end), filesep)
        folder = [folder filesep];
    end
    % Write json file
    fid = fopen([folder 'Output/Selections/' selection_name '.json'],'w');
    fprintf(fid, '%s', jsonencode(output));
    fclose(fid);
    
    disp('Selection exported')
end











